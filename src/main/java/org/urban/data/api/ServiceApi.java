/*
 * Copyright 2018 New York University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.urban.data.api;

import com.google.gson.JsonElement;
import org.urban.data.api.resources.db.Repository;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.File;
import java.io.FileReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Singleton;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.urban.data.api.resources.annotation.ColumnAnnotation;
import org.urban.data.api.resources.annotation.ColumnTermAnnotation;
import org.urban.data.api.resources.annotation.DomainAnnotation;
import org.urban.data.api.resources.annotation.DomainTermAnnotation;
import org.urban.data.api.resources.db.Column;
import org.urban.data.api.resources.db.Database;
import org.urban.data.api.resources.db.Dataset;
import org.urban.data.api.resources.db.Domain;
import org.urban.data.api.urls.UrlFactory;

/**
 * REST Web Service.
 *
 * @author Heiko Mueller <heiko.mueller@nyu.edu>
 */
@Path("/")
@Singleton
public class ServiceApi {

    private static final Logger LOGGER = Logger.getGlobal();
    
    @Context
    private UriInfo context;

    private final Repository _db;
    
    /**
     * Creates a new API service instance.
     * 
     * Reads configuration from file.
     * 
     * @throws java.lang.ClassNotFoundException
     * @throws java.io.FileNotFoundException
     * @throws java.sql.SQLException
     */
    public ServiceApi() throws java.lang.ClassNotFoundException, java.io.FileNotFoundException, java.sql.SQLException {
        
        File configFile = new File(getClass().getClassLoader().getResource("service.json").getFile());

        JsonObject config = new JsonParser()
                .parse(new FileReader(configFile))
                .getAsJsonObject();

        String url = config.get("url").getAsString();
		
        _db = new Repository(
                config.get("name").getAsString(),
                new UrlFactory(url)
        );
        for (JsonElement conf : config.get("databases").getAsJsonArray()) {
            _db.add(
                    new Database(
                            new DatabaseConfig(
                                    conf.getAsJsonObject(),
                                    url
                            )
                    )
            );
        }
    }

    /**
     * Get API service descriptor.
     * 
     * @return an instance of javax.ws.rs.core.Response
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response welcome() {

        return Response
                .ok()
                .entity(_db)
                .type(MediaType.APPLICATION_JSON)
                .build();
    }

    // -------------------------------------------------------------------------
    //
    // DATABASEs
    //
    // -------------------------------------------------------------------------
    
    /**
     * Get listing of available databases.
     * 
     * @param offset
     * @param limit
     * @param sortKey
     * @param sortDesc
     * @return an instance of javax.ws.rs.core.Response containing database
     *         listing in Json format
     */
    @GET
    @Path("/databases")
    @Produces(MediaType.APPLICATION_JSON)
    public Response listDatabases(
	@QueryParam("offset") @DefaultValue("-1") int offset,
	@QueryParam("limit") @DefaultValue("0") int limit,
	@QueryParam("sortKey") @DefaultValue(Database.KEY_NAME) String sortKey,
	@QueryParam("sortDesc") @DefaultValue("false") boolean sortDesc
    ) {
        
        try {
            return Response
                    .ok()
                    .entity(_db.databases(offset, limit, sortKey, sortDesc))
                    .type(MediaType.APPLICATION_JSON)
                    .build();
        } catch (java.lang.IllegalArgumentException ex) {
            throw new WebApplicationException(ex.toString(), 400);
        }
    }

    /**
     * Get database.
     * 
     * @param dbIdentifier
     * @return an instance of javax.ws.rs.core.Response containing database
     */
    @GET
    @Path("/databases/{dbIdentifier}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getDatabase(
	@PathParam("dbIdentifier") String dbIdentifier
    ) {
        
        return Response
                .ok()
                .entity(_db.databases(dbIdentifier))
                .type(MediaType.APPLICATION_JSON)
                    .build();
    }

    // -------------------------------------------------------------------------
    //
    // DATASETs
    //
    // -------------------------------------------------------------------------
    
    /**
     * List all datasets in a database.
     * 
     * @param dbIdentifier
     * @param offset
     * @param limit
     * @param sortKey
     * @param sortDesc
     * @param filter
     * @return
     * @throws java.io.IOException
     * @throws javax.ws.rs.WebApplicationException 
     */
    @Path("/databases/{dbIdentifier}/datasets")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listDatabaseDatasets(
	@PathParam("dbIdentifier") String dbIdentifier,
	@QueryParam("offset") @DefaultValue("-1") int offset,
	@QueryParam("limit") @DefaultValue("0") int limit,
	@QueryParam("sortKey") @DefaultValue(Dataset.KEY_NAME) String sortKey,
	@QueryParam("sortDesc") @DefaultValue("false") boolean sortDesc,
	@QueryParam("filter") @DefaultValue("") String filter
    ) throws java.io.IOException, javax.ws.rs.WebApplicationException {
	
        try {
            return Response
                    .ok()
                    .entity(_db
                            .databases(dbIdentifier)
                            .datasets(offset, limit, sortKey, sortDesc, filter)
                    )
                    .type(MediaType.APPLICATION_JSON)
                    .build();
        } catch (java.sql.SQLException ex) {
            LOGGER.log(Level.SEVERE, "LIST DATATASETS FOR DATABASE " + dbIdentifier, ex);
            throw new WebApplicationException(ex);
        } catch (java.lang.IllegalArgumentException ex) {
            throw new WebApplicationException(ex.toString(), 400);
        }
    }

    /**
     * Get dataset.
     * 
     * @param dbIdentifier
     * @param dsIdentifier
     * @return
     * @throws java.io.IOException
     * @throws javax.ws.rs.WebApplicationException 
     */
    @Path("/databases/{dbIdentifier}/datasets/{dsIdentifier}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getDataset(
	@PathParam("dbIdentifier") String dbIdentifier,
	@PathParam("dsIdentifier") String dsIdentifier
    ) throws java.io.IOException, javax.ws.rs.WebApplicationException {
	
        try {
            return Response
                    .ok()
                    .entity(_db.databases(dbIdentifier).datasets(dsIdentifier))
                    .type(MediaType.APPLICATION_JSON)
                    .build();
        } catch (java.sql.SQLException ex) {
            LOGGER.log(Level.SEVERE, "DOMAIN" + dbIdentifier, ex);
            throw new WebApplicationException(ex);
        } catch (java.lang.IllegalArgumentException ex) {
            throw new WebApplicationException(ex.toString(), 400);
        }
    }

    /**
     * List all columns in a dataset.
     * 
     * @param dbIdentifier
     * @param dsIdentifier
     * @param offset
     * @param limit
     * @param sortKey
     * @param sortDesc
     * @param filter
     * @return
     * @throws java.io.IOException
     * @throws javax.ws.rs.WebApplicationException 
     */
    @Path("/databases/{dbIdentifier}/datasets/{dsIdentifier}/columns")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listDatasetColumns(
	@PathParam("dbIdentifier") String dbIdentifier,
	@PathParam("dsIdentifier") String dsIdentifier,
	@QueryParam("offset") @DefaultValue("-1") int offset,
	@QueryParam("limit") @DefaultValue("0") int limit,
	@QueryParam("sortKey") @DefaultValue(Column.KEY_NAME) String sortKey,
	@QueryParam("sortDesc") @DefaultValue("false") boolean sortDesc,
	@QueryParam("filter") @DefaultValue("") String filter
    ) throws java.io.IOException, javax.ws.rs.WebApplicationException {
	
        try {
            return Response
                    .ok()
                    .entity(_db
                            .databases(dbIdentifier)
                            .datasets(dsIdentifier)
                            .columns(offset, limit, sortKey, sortDesc, filter)
                    )
                    .type(MediaType.APPLICATION_JSON)
                    .build();
        } catch (java.sql.SQLException ex) {
            LOGGER.log(Level.SEVERE, "LIST COLUMNS FOR DATASET " + dsIdentifier, ex);
            throw new WebApplicationException(ex);
        } catch (java.lang.IllegalArgumentException ex) {
            throw new WebApplicationException(ex.toString(), 400);
        }
    }

    
    // -------------------------------------------------------------------------
    //
    // COLUMNs
    //
    // -------------------------------------------------------------------------
    
    /**
     * List all columns in a database.
     * 
     * @param dbIdentifier
     * @param offset
     * @param limit
     * @param sortKey
     * @param sortDesc
     * @param filter
     * @return
     * @throws java.io.IOException
     * @throws javax.ws.rs.WebApplicationException 
     */
    @Path("/databases/{dbIdentifier}/columns")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listDatabaseColumns(
	@PathParam("dbIdentifier") String dbIdentifier,
	@QueryParam("offset") @DefaultValue("-1") int offset,
	@QueryParam("limit") @DefaultValue("0") int limit,
	@QueryParam("sortKey") @DefaultValue(Column.KEY_NAME) String sortKey,
	@QueryParam("sortDesc") @DefaultValue("false") boolean sortDesc,
	@QueryParam("filter") @DefaultValue("") String filter
    ) throws java.io.IOException, javax.ws.rs.WebApplicationException {
	
        try {
            return Response
                    .ok()
                    .entity(_db
                            .databases(dbIdentifier)
                            .columns(offset, limit, sortKey, sortDesc, filter)
                    )
                    .type(MediaType.APPLICATION_JSON)
                    .build();
        } catch (java.sql.SQLException ex) {
            LOGGER.log(Level.SEVERE, "LIST COLUMNS FOR DATABASE " + dbIdentifier, ex);
            throw new WebApplicationException(ex);
        } catch (java.lang.IllegalArgumentException ex) {
            throw new WebApplicationException(ex.toString(), 400);
        }
    }

    /**
     * Get column.
     * 
     * @param dbIdentifier
     * @param columnIdentifier
     * @return
     * @throws java.io.IOException
     * @throws javax.ws.rs.WebApplicationException 
     */
    @Path("/databases/{dbIdentifier}/columns/{columnIdentifier}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getColumn(
	@PathParam("dbIdentifier") String dbIdentifier,
	@PathParam("columnIdentifier") int columnIdentifier
    ) throws java.io.IOException, javax.ws.rs.WebApplicationException {
	
        try {
            return Response
                    .ok()
                    .entity(_db.databases(dbIdentifier).columns(columnIdentifier))
                    .type(MediaType.APPLICATION_JSON)
                    .build();
        } catch (java.sql.SQLException ex) {
            LOGGER.log(Level.SEVERE, "DOMAIN" + dbIdentifier, ex);
            throw new WebApplicationException(ex);
        } catch (java.lang.IllegalArgumentException ex) {
            throw new WebApplicationException(ex.toString(), 400);
        }
    }

    /**
     * Get column annotation.
     * 
     * Returns an object that contains the current annotation value (if the
     * column is annotated) and a list of annotation options.
     * 
     * @param dbIdentifier
     * @param columnIdentifier
     * @return
     * @throws java.io.IOException
     * @throws javax.ws.rs.WebApplicationException 
     */
    @Path("/databases/{dbIdentifier}/columns/{columnIdentifier}/annotations")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getColumnAnnotations(
	@PathParam("dbIdentifier") String dbIdentifier,
	@PathParam("columnIdentifier") int columnIdentifier
    ) throws java.io.IOException, javax.ws.rs.WebApplicationException {
	
        try {
            Column column;
            column = _db.databases(dbIdentifier).columns(columnIdentifier);
            return Response
                    .ok()
                    .entity(new ColumnAnnotation(column))
                    .type(MediaType.APPLICATION_JSON)
                    .build();
        } catch (java.sql.SQLException ex) {
            LOGGER.log(Level.SEVERE, "COLUMN ANNOTATIONS" + dbIdentifier, ex);
            throw new WebApplicationException(ex);
        } catch (java.lang.IllegalArgumentException ex) {
            throw new WebApplicationException(ex.toString(), 400);
        }
    }
    
    /**
     * Update annotation for a column.
     * 
     * Expects a request body with one part:
     * 
     * - value: Unique identifier of the annotation value
     * 
     * @param dbIdentifier
     * @param columnIdentifier
     * @param content
     * @return an instance of javax.ws.rs.core.Response containing the domain
     */
    @PUT
    @Path("/databases/{dbIdentifier}/columns/{columnIdentifier}/annotations")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateColumnTermAnnotations(
            @PathParam("dbIdentifier") String dbIdentifier,
            @PathParam("columnIdentifier") int columnIdentifier,
            String content
    ) {
        
	Integer annotationValue = ApiHelper.parseAnnotationRequest(content);
        try {
            Column column;
            column = _db.databases(dbIdentifier).columns(columnIdentifier);
            column.setAnnotation(annotationValue);
            return Response
                    .ok()
                    .entity(new ColumnAnnotation(column, annotationValue))
                    .type(MediaType.APPLICATION_JSON)
                    .build();
        } catch (java.sql.SQLException ex) {
            LOGGER.log(Level.SEVERE, "UPDATE COLUMN ANNOTATION", ex);
            throw new WebApplicationException(ex);
        } catch (java.lang.IllegalArgumentException ex) {
            throw new WebApplicationException(ex.toString(), 400);
        }
    }
    
    /**
     * List all domains in a column.
     * 
     * @param dbIdentifier
     * @param columnIdentifier
     * @param offset
     * @param limit
     * @param sortKey
     * @param sortDesc
     * @param filter
     * @return
     * @throws java.io.IOException
     * @throws javax.ws.rs.WebApplicationException 
     */
    @Path("/databases/{dbIdentifier}/columns/{columnIdentifier}/domains")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listColumnDomains(
            @PathParam("dbIdentifier") String dbIdentifier,
            @PathParam("columnIdentifier") int columnIdentifier,
            @QueryParam("offset") @DefaultValue("-1") int offset,
            @QueryParam("limit") @DefaultValue("0") int limit,
            @QueryParam("sortKey") @DefaultValue(Domain.KEY_TERM_COUNT) String sortKey,
            @QueryParam("sortDesc") @DefaultValue("true") boolean sortDesc,
            @QueryParam("filter") @DefaultValue("") String filter
    ) throws java.io.IOException, javax.ws.rs.WebApplicationException {
	
        try {
            return Response
                    .ok()
                    .entity(_db
                            .databases(dbIdentifier)
                            .columns(columnIdentifier)
                            .domains(offset, limit, sortKey, sortDesc, filter)
                    )
                    .type(MediaType.APPLICATION_JSON)
                    .build();
        } catch (java.sql.SQLException ex) {
            LOGGER.log(Level.SEVERE, "LIST DOMAINS FOR DATABASE " + dbIdentifier, ex);
            throw new WebApplicationException(ex);
        } catch (java.lang.IllegalArgumentException ex) {
            throw new WebApplicationException(ex.toString(), 400);
        }
    }


    /**
     * List terms in a database column.
     * 
     * @param dbIdentifier
     * @param columnIdentifier
     * @param offset
     * @param limit
     * @param sortDesc
     * @param filter
     * @return
     * @throws java.io.IOException
     * @throws javax.ws.rs.WebApplicationException 
     */
    @Path("/databases/{dbIdentifier}/columns/{columnIdentifier}/terms")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listColumnTerms(
	@PathParam("dbIdentifier") String dbIdentifier,
	@PathParam("columnIdentifier") int columnIdentifier,
	@QueryParam("offset") @DefaultValue("-1") int offset,
	@QueryParam("limit") @DefaultValue("0") int limit,
	@QueryParam("sortDesc") @DefaultValue("false") boolean sortDesc,
	@QueryParam("filter") @DefaultValue("") String filter
    ) throws java.io.IOException, javax.ws.rs.WebApplicationException {
	
        try {
            return Response
                    .ok()
                    .entity(_db
                            .databases(dbIdentifier)
                            .columns(columnIdentifier)
                            .terms(offset, limit, sortDesc, filter)
                    )
                    .type(MediaType.APPLICATION_JSON)
                    .build();
        } catch (java.sql.SQLException ex) {
            LOGGER.log(Level.SEVERE, "LIST TERMS FOR COLUMN " + columnIdentifier, ex);
            throw new WebApplicationException(ex);
        }
    }

    /**
     * Get column term annotation.
     * 
     * Returns an object that contains the current annotation value (if the
     * term is annotated) and a list of annotation options.
     * 
     * @param dbIdentifier
     * @param columnIdentifier
     * @param termId
     * @return
     * @throws java.io.IOException
     * @throws javax.ws.rs.WebApplicationException 
     */
    @Path("/databases/{dbIdentifier}/columns/{columnIdentifier}/terms/{termId}/annotations")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getColumnTermAnnotations(
	@PathParam("dbIdentifier") String dbIdentifier,
	@PathParam("columnIdentifier") int columnIdentifier,
	@PathParam("termId") int termId
    ) throws java.io.IOException, javax.ws.rs.WebApplicationException {
	
        try {
            Column column;
            column = _db.databases(dbIdentifier).columns(columnIdentifier);
            return Response
                    .ok()
                    .entity(new ColumnTermAnnotation(column, termId))
                    .type(MediaType.APPLICATION_JSON)
                    .build();
        } catch (java.sql.SQLException ex) {
            LOGGER.log(Level.SEVERE, "COLUMN TERM ANNOTATIONS" + dbIdentifier, ex);
            throw new WebApplicationException(ex);
        } catch (java.lang.IllegalArgumentException ex) {
            throw new WebApplicationException(ex.toString(), 400);
        }
    }

    /**
     * Update annotation for a column term.
     * 
     * Expects a request body with one part:
     * 
     * - value: Unique identifier of the annotation value
     * 
     * @param dbIdentifier
     * @param columnIdentifier
     * @param termId
     * @param content
     * @return an instance of javax.ws.rs.core.Response containing the domain
     */
    @PUT
    @Path("/databases/{dbIdentifier}/columns/{columnIdentifier}/terms/{termId}/annotations")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateColumnTermAnnotations(
            @PathParam("dbIdentifier") String dbIdentifier,
            @PathParam("columnIdentifier") int columnIdentifier,
            @PathParam("termId") int termId,
            String content
    ) {
        
	Integer annotationValue = ApiHelper.parseAnnotationRequest(content);
        try {
            Column column;
            column = _db.databases(dbIdentifier).columns(columnIdentifier);
            column.setAnnotation(termId, annotationValue);
            return Response
                    .ok()
                    .entity(new ColumnTermAnnotation(column, termId, annotationValue))
                    .type(MediaType.APPLICATION_JSON)
                    .build();
        } catch (java.sql.SQLException ex) {
            LOGGER.log(Level.SEVERE, "UPDATE COLUMN TERM ANNOTATION", ex);
            throw new WebApplicationException(ex);
        } catch (java.lang.IllegalArgumentException ex) {
            throw new WebApplicationException(ex.toString(), 400);
        }
    }

    
    // -------------------------------------------------------------------------
    //
    // DOMAINs
    //
    // -------------------------------------------------------------------------
    
    /**
     * Retrieve a particular domain.
     * 
     * Expects a request body with two parts:
     * 
     * - database: Unique database identifier
     * - domain: Database-specific domain identifier
     * 
     * @param content
     * @return an instance of javax.ws.rs.core.Response containing the domain
     */
    @POST
    @Path("/domains")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getDatabaseDomain(String content) {
        
        JsonObject body = new JsonParser().parse(content).getAsJsonObject();
        String databaseId;
        if (body.has("database")) {
            databaseId = body.get("database").getAsString();
        } else {
            throw new WebApplicationException("Missing key database in request body", 400);
        }
        int domainId;
        if (body.has("domain")) {
            domainId = body.get("domain").getAsInt();
        } else {
            throw new WebApplicationException("Missing key domain in request body", 400);
        }
        
        try {
            return Response
                    .ok()
                    .entity(_db.databases(databaseId).domains(domainId))
                    .type(MediaType.APPLICATION_JSON)
                    .build();
        } catch (java.sql.SQLException ex) {
            LOGGER.log(Level.SEVERE, "GET DATABASE DOMAIN", ex);
            throw new WebApplicationException(ex);
        } catch (java.lang.IllegalArgumentException ex) {
            throw new WebApplicationException(ex.toString(), 400);
        }
    }

    /**
     * List all domains in a database.
     * 
     * @param dbIdentifier
     * @param offset
     * @param limit
     * @param sortKey
     * @param sortDesc
     * @param filter
     * @return
     * @throws java.io.IOException
     * @throws javax.ws.rs.WebApplicationException 
     */
    @Path("/databases/{dbIdentifier}/domains")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listDatabaseDomains(
	@PathParam("dbIdentifier") String dbIdentifier,
	@QueryParam("offset") @DefaultValue("-1") int offset,
	@QueryParam("limit") @DefaultValue("0") int limit,
	@QueryParam("sortKey") @DefaultValue(Domain.KEY_COLUMN_COUNT) String sortKey,
	@QueryParam("sortDesc") @DefaultValue("true") boolean sortDesc,
	@QueryParam("filter") @DefaultValue("") String filter
    ) throws java.io.IOException, javax.ws.rs.WebApplicationException {
	
        try {
            return Response
                    .ok()
                    .entity(_db
                            .databases(dbIdentifier)
                            .domains(offset, limit, sortKey, sortDesc, filter)
                    )
                    .type(MediaType.APPLICATION_JSON)
                    .build();
        } catch (java.sql.SQLException ex) {
            LOGGER.log(Level.SEVERE, "LIST DOMAINS FOR DATABASE " + dbIdentifier, ex);
            throw new WebApplicationException(ex);
        } catch (java.lang.IllegalArgumentException ex) {
            throw new WebApplicationException(ex.toString(), 400);
        }
    }

    /**
     * Get domain.
     * 
     * @param dbIdentifier
     * @param domainIdentifier
     * @return
     * @throws java.io.IOException
     * @throws javax.ws.rs.WebApplicationException 
     */
    @Path("/databases/{dbIdentifier}/domains/{domainIdentifier}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getDomain(
	@PathParam("dbIdentifier") String dbIdentifier,
	@PathParam("domainIdentifier") int domainIdentifier
    ) throws java.io.IOException, javax.ws.rs.WebApplicationException {
	
        try {
            return Response
                    .ok()
                    .entity(_db.databases(dbIdentifier).domains(domainIdentifier))
                    .type(MediaType.APPLICATION_JSON)
                    .build();
        } catch (java.sql.SQLException ex) {
            LOGGER.log(Level.SEVERE, "DOMAIN" + dbIdentifier, ex);
            throw new WebApplicationException(ex);
        } catch (java.lang.IllegalArgumentException ex) {
            throw new WebApplicationException(ex.toString(), 400);
        }
    }

    /**
     * Get domain annotation.
     * 
     * Returns an object that contains the current annotation value (if the
     * domain is annotated) and a list of annotation options.
     * 
     * @param dbIdentifier
     * @param domainIdentifier
     * @return
     * @throws java.io.IOException
     * @throws javax.ws.rs.WebApplicationException 
     */
    @Path("/databases/{dbIdentifier}/domains/{domainIdentifier}/annotations")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getDomainAnnotations(
	@PathParam("dbIdentifier") String dbIdentifier,
	@PathParam("domainIdentifier") int domainIdentifier
    ) throws java.io.IOException, javax.ws.rs.WebApplicationException {
	
        try {
            Domain domain;
            domain = _db.databases(dbIdentifier).domains(domainIdentifier);
            return Response
                    .ok()
                    .entity(new DomainAnnotation(domain))
                    .type(MediaType.APPLICATION_JSON)
                    .build();
        } catch (java.sql.SQLException ex) {
            LOGGER.log(Level.SEVERE, "DOMAIN ANNOTATIONS" + dbIdentifier, ex);
            throw new WebApplicationException(ex);
        } catch (java.lang.IllegalArgumentException ex) {
            throw new WebApplicationException(ex.toString(), 400);
        }
    }

    /**
     * Update annotation for a domain.
     * 
     * Expects a request body with one part:
     * 
     * - value: Unique identifier of the annotation value
     * 
     * @param dbIdentifier
     * @param domainIdentifier
     * @param content
     * @return an instance of javax.ws.rs.core.Response containing the domain
     */
    @PUT
    @Path("/databases/{dbIdentifier}/domains/{domainIdentifier}/annotations")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateDomainAnnotations(
            @PathParam("dbIdentifier") String dbIdentifier,
            @PathParam("domainIdentifier") int domainIdentifier,
            String content
    ) {
        
	Integer annotationValue = ApiHelper.parseAnnotationRequest(content);
        try {
            Domain domain;
            domain = _db.databases(dbIdentifier).domains(domainIdentifier);
            domain.setAnnotation(annotationValue);
            return Response
                    .ok()
                    .entity(new DomainAnnotation(domain, annotationValue))
                    .type(MediaType.APPLICATION_JSON)
                    .build();
        } catch (java.sql.SQLException ex) {
            LOGGER.log(Level.SEVERE, "UPDATE DOMAIN ANNOTATION", ex);
            throw new WebApplicationException(ex);
        } catch (java.lang.IllegalArgumentException ex) {
            throw new WebApplicationException(ex.toString(), 400);
        }
    }
    
    /**
     * List columns a domain occurs in.
     * 
     * @param dbIdentifier
     * @param domainIdentifier
     * @param offset
     * @param limit
     * @param sortKey
     * @param sortDesc
     * @param filter
     * @return
     * @throws java.io.IOException
     * @throws javax.ws.rs.WebApplicationException 
     */
    @Path("/databases/{dbIdentifier}/domains/{domainIdentifier}/columns")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listDomainColumns(
	@PathParam("dbIdentifier") String dbIdentifier,
	@PathParam("domainIdentifier") int domainIdentifier,
	@QueryParam("offset") @DefaultValue("-1") int offset,
	@QueryParam("limit") @DefaultValue("0") int limit,
	@QueryParam("sortKey") @DefaultValue(Column.KEY_NAME) String sortKey,
	@QueryParam("sortDesc") @DefaultValue("false") boolean sortDesc,
	@QueryParam("filter") @DefaultValue("") String filter
    ) throws java.io.IOException, javax.ws.rs.WebApplicationException {
	
        try {
            return Response
                    .ok()
                    .entity(_db
                            .databases(dbIdentifier)
                            .domains(domainIdentifier)
                            .columns(offset, limit, sortKey, sortDesc, filter)
                    )
                    .type(MediaType.APPLICATION_JSON)
                    .build();
        } catch (java.sql.SQLException ex) {
            LOGGER.log(Level.SEVERE, "LIST COLUMNS FOR DOMAIN " + domainIdentifier, ex);
            throw new WebApplicationException(ex);
        } catch (java.lang.IllegalArgumentException ex) {
            throw new WebApplicationException(ex.toString(), 400);
        }
    }
    
    /**
     * List terms in a database domain.
     * 
     * @param dbIdentifier
     * @param domainIdentifier
     * @param offset
     * @param limit
     * @param sortDesc
     * @param filter
     * @return
     * @throws java.io.IOException
     * @throws javax.ws.rs.WebApplicationException 
     */
    @Path("/databases/{dbIdentifier}/domains/{domainIdentifier}/terms")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listDomainTerms(
	@PathParam("dbIdentifier") String dbIdentifier,
	@PathParam("domainIdentifier") int domainIdentifier,
	@QueryParam("offset") @DefaultValue("-1") int offset,
	@QueryParam("limit") @DefaultValue("0") int limit,
	@QueryParam("sortDesc") @DefaultValue("false") boolean sortDesc,
	@QueryParam("filter") @DefaultValue("") String filter
    ) throws java.io.IOException, javax.ws.rs.WebApplicationException {
	
        try {
            return Response
                    .ok()
                    .entity(_db
                            .databases(dbIdentifier)
                            .domains(domainIdentifier)
                            .terms(offset, limit, sortDesc, filter)
                    )
                    .type(MediaType.APPLICATION_JSON)
                    .build();
        } catch (java.sql.SQLException ex) {
            LOGGER.log(Level.SEVERE, "LIST TERMS FOR DOMAIN " + domainIdentifier, ex);
            throw new WebApplicationException(ex);
        }
    }

    /**
     * Get domain term annotation.
     * 
     * Returns an object that contains the current annotation value (if the
     * term is annotated) and a list of annotation options.
     * 
     * @param dbIdentifier
     * @param domainIdentifier
     * @param termId
     * @return
     * @throws java.io.IOException
     * @throws javax.ws.rs.WebApplicationException 
     */
    @Path("/databases/{dbIdentifier}/domains/{domainIdentifier}/terms/{termId}/annotations")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getDomainTermAnnotations(
	@PathParam("dbIdentifier") String dbIdentifier,
	@PathParam("domainIdentifier") int domainIdentifier,
	@PathParam("termId") int termId
    ) throws java.io.IOException, javax.ws.rs.WebApplicationException {
	
        try {
            Domain domain;
            domain = _db.databases(dbIdentifier).domains(domainIdentifier);
            return Response
                    .ok()
                    .entity(new DomainTermAnnotation(domain, termId))
                    .type(MediaType.APPLICATION_JSON)
                    .build();
        } catch (java.sql.SQLException ex) {
            LOGGER.log(Level.SEVERE, "DOMAIN TERM ANNOTATIONS" + dbIdentifier, ex);
            throw new WebApplicationException(ex);
        } catch (java.lang.IllegalArgumentException ex) {
            throw new WebApplicationException(ex.toString(), 400);
        }
    }

    /**
     * Update annotation for a domain term.
     * 
     * Expects a request body with one part:
     * 
     * - value: Unique identifier of the annotation value
     * 
     * @param dbIdentifier
     * @param domainIdentifier
     * @param termId
     * @param content
     * @return an instance of javax.ws.rs.core.Response containing the domain
     */
    @PUT
    @Path("/databases/{dbIdentifier}/domains/{domainIdentifier}/terms/{termId}/annotations")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateDomainTermAnnotations(
            @PathParam("dbIdentifier") String dbIdentifier,
            @PathParam("domainIdentifier") int domainIdentifier,
            @PathParam("termId") int termId,
            String content
    ) {
        
	Integer annotationValue = ApiHelper.parseAnnotationRequest(content);
        try {
            Domain domain;
            domain = _db.databases(dbIdentifier).domains(domainIdentifier);
            domain.setAnnotation(termId, annotationValue);
            return Response
                    .ok()
                    .entity(new DomainTermAnnotation(domain, termId, annotationValue))
                    .type(MediaType.APPLICATION_JSON)
                    .build();
        } catch (java.sql.SQLException ex) {
            LOGGER.log(Level.SEVERE, "UPDATE DOMAIN TERM ANNOTATION", ex);
            throw new WebApplicationException(ex);
        } catch (java.lang.IllegalArgumentException ex) {
            throw new WebApplicationException(ex.toString(), 400);
        }
    }
}
