/*
 * Copyright 2018 New York University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.urban.data.api.resources.db;

import com.google.gson.stream.JsonWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.StreamingOutput;
import org.urban.data.api.resources.HATEOASWriter;
import org.urban.data.api.resources.ResourceListing;
import org.urban.data.api.urls.UrlFactory;

/**
 * Database repository is a collection of databases that are available via the
 * API.
 * 
 * @author Heiko Mueller <heiko.mueller@nyu.edu>
 */
public class Repository implements StreamingOutput {
    
    private final HashMap<String, Database> _databases;
    private final String _name;
    private final Date _startedAt;
    private final UrlFactory _urls;
    
    public Repository(String name, UrlFactory urls) {
        
        _databases = new HashMap<>();
        _name = name;
        _startedAt = new Date();
        _urls = urls;
    }
    
    public final Repository add(Database db) {
        
        _databases.put(db.identifier(), db);
        
        return this;
    }
    
    /**
     * Get database listing.
     * 
     * Supports the following sort keys: name, columnCount, datasetCount, and
     * domainCount.
     * 
     * @param offset
     * @param limit
     * @param sortKey
     * @param sortDesc
     * @return 
     */
    public ResourceListing<Database> databases(
            int offset,
            int limit,
            String sortKey,
            boolean sortDesc
    ) {
        
        ResourceListing<Database> result;
        result = new ResourceListing<>(
                "databases",
                offset,
                limit,
                _databases.size(),
                _urls.getDatabasesUrl(),
                sortKey,
                sortDesc,
		""
        );
        
        List<Database> databases = new ArrayList<>(_databases.values());
        if (sortKey.equalsIgnoreCase(Database.KEY_NAME)) {
            Collections.sort(databases, new Comparator<Database>(){
                @Override
                public int compare(Database db1, Database db2) {
                    int comp = db1.name().compareTo(db2.name());
                    if (comp == 0) {
                        comp = db1.title().compareTo(db2.title());
                    }
                    return comp;
                }
            });
        } else if (sortKey.equalsIgnoreCase(Database.KEY_TITLE)) {
            Collections.sort(databases, new Comparator<Database>(){
                @Override
                public int compare(Database db1, Database db2) {
                    int comp = db1.title().compareTo(db2.title());
                    if (comp == 0) {
                        comp = db1.name().compareTo(db2.name());
                    }
                    return comp;
                }
            });
        } else if (sortKey.equalsIgnoreCase(Database.KEY_COLUMN_COUNT)) {
            Collections.sort(databases, new Comparator<Database>(){
                @Override
                public int compare(Database db1, Database db2) {
                    return Integer.compare(db1.columnCount(), db2.columnCount());
                }
            });
        } else if (sortKey.equalsIgnoreCase(Database.KEY_DATASET_COUNT)) {
            Collections.sort(databases, new Comparator<Database>(){
                @Override
                public int compare(Database db1, Database db2) {
                    return Integer.compare(db1.datasetCount(), db2.datasetCount());
                }
            });
        } else if (sortKey.equalsIgnoreCase(Database.KEY_DOMAIN_COUNT)) {
            Collections.sort(databases, new Comparator<Database>(){
                @Override
                public int compare(Database db1, Database db2) {
                    return Integer.compare(db1.domainCount(), db2.domainCount());
                }
            });
        } else {
            throw new java.lang.IllegalArgumentException("Invalid sort key: " + sortKey);
        }
        if (sortDesc) {
            Collections.reverse(databases);
        }
        
        for (int iDb = Math.max(0, offset); iDb < databases.size(); iDb++) {
            if ((limit > 0) && (result.size() >= limit)) {
                break;
            }
            
            result.add(databases.get(iDb));
        }
        
        return result;
    }
    
    public Database databases(String identifier) throws javax.ws.rs.WebApplicationException {
        
        if (_databases.containsKey(identifier)) {
            return _databases.get(identifier);
        } else {
            throw new WebApplicationException(404);
        }
    }
    
    @Override
    public void write(OutputStream out) throws IOException, WebApplicationException {

	try (JsonWriter writer = new JsonWriter(new OutputStreamWriter(out, "UTF-8"))) {
	    writer.beginObject();
            writer.name("name").value(_name);
            writer.name("startedAt").value(_startedAt.toString());
            new HATEOASWriter()
                    .add("self", _urls.getWelcomeUrl())
                    .add("databases", _urls.getDatabasesUrl())
                    .write(writer);
            writer.endObject();
        }
    }
}
