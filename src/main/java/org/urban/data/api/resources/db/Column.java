/*
 * Copyright 2018 New York University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.urban.data.api.resources.db;

import org.urban.data.api.DatabaseConfig;
import com.google.gson.stream.JsonWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import org.urban.data.api.jdbc.JDBCHelper;
import org.urban.data.api.resources.ApiResource;
import org.urban.data.api.resources.DatabaseResource;
import org.urban.data.api.resources.HATEOASWriter;
import org.urban.data.api.resources.ResourceListing;
import org.urban.data.api.resources.TermHATEOASFactory;

/**
 * Column in a database.
 * 
 * Each column has a unique identifier and a name. Each column is part of a
 * dataset.
 * 
 * @author Heiko Mueller <heiko.mueller@nyu.edu>
 */
public class Column extends DatabaseResource {

    public static final String KEY_DATASET = "dataset";
    public static final String KEY_NAME = "name";
    public static final String KEY_TERM_COUNT = "termCount";
    
    private class ColumnTermReferenceFactory implements TermHATEOASFactory {

	private final DatabaseConfig _config;
	private final Column _column;
	
	public ColumnTermReferenceFactory(Column column, DatabaseConfig config) {

	    _column = column;
	    _config = config;
	}
	
	@Override
	public HATEOASWriter getWriter(int termId) {

	    return new HATEOASWriter()
		    .add(
			    "annotations",
			    _config
				    .urls()
				    .getColumnTermAnnotationsUrl(
					    _column.identifier(),
					    termId
				    )
		    );
	}
    }
    
    private final DatabaseConfig _config;
    private final Dataset _dataset;
    private final int _identifier;
    private final String _name;
    private final int _termCount;
    
    public Column(
            int identifier,
            String name,
            Dataset dataset,
            int termCount,
            DatabaseConfig config
    ) {
        super(config);
        _identifier = identifier;
        _name = name;
        _dataset = dataset;
        _termCount = termCount;
        _config = config;
    }

    private ResourceListing<Term> addTermsFromResultSet(
            ResultSet rs,
            ResourceListing<Term> terms
    ) throws java.sql.SQLException {
        
        while (rs.next()) {
            terms.add(
		    new Term(
			    rs.getInt(1),
			    rs.getString(2),
			    new ColumnTermReferenceFactory(this, _config)
		    )
	    );
        }
        return terms;
    }
    
    public ResourceListing<Domain> domains(
            int offset,
            int limit,
            String sortKey,
            boolean sortDesc,
            String filter
    ) throws java.sql.SQLException {
        
        String sortColumn;
        if (sortKey.equalsIgnoreCase(Domain.KEY_COLUMN_COUNT)) {
            sortColumn = "column_count";
        } else if (sortKey.equalsIgnoreCase(Domain.KEY_TERM_COUNT)) {
            sortColumn = "term_count";
        } else {
            throw new java.lang.IllegalArgumentException("Invalid sort key: " + sortKey);
        }

	if (!filter.trim().equals("")) {
	    return this.queryDomains(
		    offset,
		    limit,
		    sortKey,
		    sortColumn,
		    sortDesc,
		    filter
	    );
	} else {
	    return this.fetchDomains(
		    offset,
		    limit,
		    sortKey,
		    sortColumn,
		    sortDesc,
		    filter
	    );
	}
    }

    private ResourceListing<Domain> fetchDomains(
            int offset,
            int limit,
            String sortKey,
            String sortColumn,
            boolean sortDesc,
            String filter
    ) throws java.sql.SQLException {

	String sql = 
                "FROM " + _config.getDomainsTable() + " d, " + _config.getDomainColumnsTable() + " c " +
                "WHERE d.domain_id = c.domain_id AND c.column_id = " + _identifier;

        String query = ApiResource.decorateForPagination(
                "SELECT d.domain_id, d.column_count, d.term_count, d.is_strong " +
                    sql + " ORDER BY " + sortColumn,
                offset,
                limit,
                sortDesc
        );

        try (
                Connection con = _config.getConnection();
                Statement stmt = con.createStatement();
                ResultSet rs = stmt.executeQuery(query)
        ) {
	    return Domain.addDomainsFromResultSet(
		    rs,
		    new ResourceListing<Domain>(
			    "domains",
			    offset,
			    limit,
			    JDBCHelper.getCount(con, "SELECT COUNT(*) " + sql),
			    _config.urls().getColumnDomainsUrl(_identifier),
			    sortKey,
			    sortDesc,
			    filter
		    ),
                    _config
	    );
        }
    }
    
    public Integer getAnnotation() throws java.sql.SQLException {
        
        return this.getAnnotation(this.getUri());
    }
    
    public Integer getAnnotation(int termId) throws java.sql.SQLException {
        
        return this.getAnnotation(this.getUri(termId));
    }
    
    private String getUri() {
    
        return "urban:column:" + _identifier;
    }
    
    private String getUri(int termId) {
    
        return this.getUri() + ":" + termId;
    }
    
    public int identifier() {
        
        return _identifier;
    }

    private ResourceListing<Domain> queryDomains(
            int offset,
            int limit,
            String sortKey,
            String sortColumn,
            boolean sortDesc,
            String filter
    ) throws java.sql.SQLException {

	String sql = "SELECT d.domain_id, d.column_count, d.term_count, d.is_strong " +
                "FROM " +
		    _config.getDomainsTable() + " d, " +
		    _config.getDomainColumnsTable() + " c, " +
		    _config.getDomainTermsTable() + " m, " +
		    _config.getTermsTable() + " t " +
		"WHERE d.domain_id = c.domain_id AND c.column_id = " + _identifier +
                " AND c.domain_id = m.domain_id AND m.term_id = t.id AND LOWER(t.value) LIKE ? " +
		"GROUP BY d.domain_id, d.column_count, d.term_count";
                
        String query = ApiResource.decorateForPagination(
		sql + " ORDER BY " + sortColumn,
		offset,
		limit,
		sortDesc
	);

	String para = filter.toLowerCase();
	
        try (
                Connection con = _config.getConnection();
                PreparedStatement pStmt = con.prepareStatement(query);
        ) {
	    pStmt.setString(1, para);
	    try (ResultSet rs = pStmt.executeQuery()) {
		return Domain.addDomainsFromResultSet(
			rs,
			new ResourceListing<Domain>(
				"domains",
				offset,
				limit,
				JDBCHelper.getCount(con, "SELECT COUNT(*) FROM (" + sql + ") AS q", para),
				_config.urls().getColumnDomainsUrl(_identifier),
				sortKey,
				sortDesc,
				filter
			),
                        _config
		);
	    }
        }
    }
    
    public ResourceListing<Term> terms(
            int offset,
            int limit,
            boolean sortDesc,
            String filter
    ) throws java.sql.SQLException {
        
        String sql = 
                "FROM " + _config.getTermsTable() + " t, " +
                _config.getColumnTermsTable() + " c " +
                "WHERE t.id = c.term_id AND c.column_id = " + _identifier;
        
        if (!filter.trim().equals("")) {
            String para = filter.toLowerCase();
            try (Connection con = _config.getConnection()) {
                sql += " AND LOWER(t.value) LIKE ?";
                String query = ApiResource.decorateForPagination(
                        "SELECT t.id, t.value " + sql + " ORDER BY t.value",
                        offset,
                        limit,
                        sortDesc
                );
                try (PreparedStatement pStmt = con.prepareStatement(query)) {
                    pStmt.setString(1, para);
                    try (ResultSet rs = pStmt.executeQuery()) {
                        return this.addTermsFromResultSet(
                                rs,
                                new ResourceListing<Term>(
                                        "terms",
                                        offset,
                                        limit,
                                        JDBCHelper.getCount(con, "SELECT COUNT(*) " + sql, para),
                                        _config.urls().getColumnTermsUrl(_identifier),
                                        null,
                                        sortDesc,
                                        filter
                                )
                        );
                    }
                }
            }
        } else {
            String query = ApiResource.decorateForPagination(
                    "SELECT t.id, t.value " + sql + " ORDER BY t.value",
                    offset,
                    limit,
                    sortDesc
            );
            try (
                    Connection con = _config.getConnection();
                    Statement stmt = con.createStatement();
                    ResultSet rs = stmt.executeQuery(query);
            ) {
                return this.addTermsFromResultSet(
                        rs,
                        new ResourceListing<Term>(
                                "terms",
                                offset,
                                limit,
                                _termCount,
                                _config.urls().getColumnTermsUrl(_identifier),
                                null,
                                sortDesc,
                                filter
                        )
                );
            }
        }
    }
    
    public void setAnnotation(Integer value) throws java.sql.SQLException {
        
        this.setAnnotation(this.getUri(), value);
    }
    
    public void setAnnotation(int termId, Integer value) throws java.sql.SQLException {
        
        this.setAnnotation(this.getUri(termId), value);
    }
    
    @Override
    public void write(JsonWriter out) throws java.io.IOException {

        out.beginObject();
        out.name("id").value(_identifier);
        out.name(KEY_NAME).value(_name);
        out.name(KEY_DATASET);
        out.beginObject();
        out.name("id").value(_dataset.identifier());
        out.name("name").value(_dataset.name());
        new HATEOASWriter()
                .add("columns", _config.urls().getDatasetColumnsUrl(_dataset.identifier()))
                .add("self", _config.urls().getDatasetUrl(_dataset.identifier()))
                .write(out);
        out.endObject();
        out.name(KEY_TERM_COUNT).value(_termCount);
        new HATEOASWriter()
                .add("annotations", _config.urls().getColumnAnnotationsUrl(_identifier))
                .add("dataset", _config.urls().getDatasetUrl(_dataset.identifier()))
                .add("domains", _config.urls().getColumnDomainsUrl(_identifier))
                .add("self", _config.urls().getColumnUrl(_identifier))
                .add("terms", _config.urls().getColumnTermsUrl(_identifier))
                .write(out);
        out.endObject();
    }
}
