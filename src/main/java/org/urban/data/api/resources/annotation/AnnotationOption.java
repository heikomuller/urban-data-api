/*
 * Copyright 2018 New York University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.urban.data.api.resources.annotation;

import com.google.gson.stream.JsonWriter;
import org.urban.data.api.resources.ApiResource;

/**
 * Annotation object for a column.
 * 
 * @author Heiko Mueller <heiko.mueller@nyu.edu>
 */
public class AnnotationOption extends ApiResource {

    private final int _value;
    private final String _text;
    
    public AnnotationOption(int value, String text) {
        
        _value = value;
        _text = text;
    }

    @Override
    public void write(JsonWriter out) throws java.io.IOException {

        out.beginObject();
        out.name("value").value(_value);
        out.name("text").value(_text);
        out.endObject();
    }
}
