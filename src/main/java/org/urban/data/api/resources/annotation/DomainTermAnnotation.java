/*
 * Copyright 2018 New York University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.urban.data.api.resources.annotation;

import com.google.gson.stream.JsonWriter;
import org.urban.data.api.resources.ApiResource;
import org.urban.data.api.resources.db.Domain;

/**
 * Annotation object for a domain term.
 * 
 * @author Heiko Mueller <heiko.mueller@nyu.edu>
 */
public class DomainTermAnnotation extends ApiResource {

    private final Integer _annotation;
    private final Domain _domain;
    private final int _termId;
    
    public DomainTermAnnotation(Domain domain, int termId) throws java.sql.SQLException {
        
        _domain = domain;
        _termId = termId;
        _annotation = domain.getAnnotation(termId);
    }

    public DomainTermAnnotation(Domain domain, int termId, Integer annotation) {
        
        _domain = domain;
        _termId = termId;
        _annotation = annotation;
    }

    @Override
    public void write(JsonWriter out) throws java.io.IOException {

        out.beginObject();
        out.name("domain");
        _domain.write(out);
        out.name("term").value(_termId);
        if (_annotation != null) {
            out.name("value").value(_annotation);
        }
        out.name("options");
        out.beginArray();
        new AnnotationOption(0, "True Positive").write(out);
        new AnnotationOption(1, "False Positive").write(out);
        out.endArray();
        out.endObject();
    }
}
