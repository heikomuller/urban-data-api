/*
 * Copyright 2018 New York University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.urban.data.api.resources.annotation;

import com.google.gson.stream.JsonWriter;
import org.urban.data.api.resources.ApiResource;
import org.urban.data.api.resources.db.Column;

/**
 * Annotation object for a column.
 * 
 * @author Heiko Mueller <heiko.mueller@nyu.edu>
 */
public class ColumnAnnotation extends ApiResource {

    private final Integer _annotation;
    private final Column _column;
    
    public ColumnAnnotation(Column column) throws java.sql.SQLException {
        
        _column = column;
        _annotation = column.getAnnotation();
    }

    public ColumnAnnotation(Column column, Integer annotation) throws java.sql.SQLException {
        
        _column = column;
        _annotation = annotation;
    }

    @Override
    public void write(JsonWriter out) throws java.io.IOException {

        out.beginObject();
        out.name("column");
        _column.write(out);
        if (_annotation != null) {
            out.name("value").value(_annotation);
        }
        out.name("options");
        out.beginArray();
        new AnnotationOption(0, "Homogeneous").write(out);
        new AnnotationOption(1, "Homogeneous with NULL value").write(out);
        new AnnotationOption(2, "Homogeneous with Outliers").write(out);
        new AnnotationOption(3, "Heterogeneous").write(out);
        new AnnotationOption(4, "Heterogeneous with NULL value").write(out);
        new AnnotationOption(5, "Heterogeneous with Outliers").write(out);
        out.endArray();
        out.endObject();
    }
}
