/*
 * Copyright 2018 New York University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.urban.data.api.resources;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import org.urban.data.api.DatabaseConfig;

/**
 * Resource in a database.
 * 
 * Database resources can be annotated or contain resources that are annotated.
 * 
 * @author Heiko Mueller <heiko.mueller@nyu.edu>
 */
public abstract class DatabaseResource extends ApiResource {

    private final DatabaseConfig _config;
    
    public DatabaseResource(DatabaseConfig config) {
    
        _config = config;
    }
    
    public Integer getAnnotation(String uri) throws java.sql.SQLException {
        
        String query = "SELECT value FROM " + _config.getAnnotationsTable()
                + " WHERE uri = '" + uri + "'";
        
        try (
                Connection con = _config.getConnection();
                Statement stmt = con.createStatement();
                ResultSet rs = stmt.executeQuery(query);
        ) {
            if (rs.next()) {
                return rs.getInt(1);
            } else {
                return null;
            }
        }
    }
    
    public void setAnnotation(String uri, Integer value) throws java.sql.SQLException {
        
        try (
                Connection con = _config.getConnection();
                Statement stmt = con.createStatement();
        ) {
            if (value == null) {
                stmt.execute(
                        "DELETE FROM " + _config.getAnnotationsTable() +
                        " WHERE uri = '" + uri + "'"
                );
            } else {
                stmt.execute(
                        "INSERT INTO " + _config.getAnnotationsTable() +
                        "(uri, value) VALUES('" + uri + "', " + value + ") "+
                        "ON CONFLICT (uri) DO UPDATE SET value = " + value
                );
            }
        }
    }
}
