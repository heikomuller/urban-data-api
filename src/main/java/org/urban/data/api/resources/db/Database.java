/*
 * Copyright 2018 New York University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.urban.data.api.resources.db;

import org.urban.data.api.DatabaseConfig;
import com.google.gson.stream.JsonWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.StreamingOutput;
import org.urban.data.api.jdbc.JDBCHelper;
import org.urban.data.api.resources.ApiResource;
import org.urban.data.api.resources.HATEOASWriter;
import org.urban.data.api.resources.ResourceListing;

/**
 * A database resource represents a data source that contains one or more
 * datasets.
 * 
 * Each database is a collection of tables on which analytics are run. The
 * database has a unique identifier, a domain (e.g., data.cityofnewyork.us), and
 * a short descriptor (name).
 * 
 * @author Heiko Mueller <heiko.mueller@nyu.edu>
 */
public class Database extends ApiResource implements StreamingOutput {

    public static final String KEY_COLUMN_COUNT = "columnCount";
    public static final String KEY_DATASET_COUNT = "datasetCount";
    public static final String KEY_DOMAIN_COUNT = "domainCount";
    public static final String KEY_NAME = "name";
    public static final String KEY_TITLE = "title";
    
    private final DatabaseConfig _config;
    
    public Database(DatabaseConfig config) {
        
        _config = config;
    }

    public ResourceListing<Column> columns(
            int offset,
            int limit,
            String sortKey,
            boolean sortDesc,
            String filter
    ) throws java.sql.SQLException {
        

        String sortColumn;
        if (sortKey.equalsIgnoreCase(Column.KEY_NAME)) {
            sortColumn = "c.name";
        } else if (sortKey.equalsIgnoreCase(Column.KEY_DATASET)) {
            sortColumn = "d.name";
        } else if (sortKey.equalsIgnoreCase(Column.KEY_TERM_COUNT)) {
            sortColumn = "c.term_count";
        } else {
            throw new java.lang.IllegalArgumentException("Invalid sort key: " + sortKey);
        }
        
        
        if (!filter.trim().equals("")) {
            return this.queryColumns(
                    offset,
                    limit,
                    sortKey,
                    sortColumn,
                    sortDesc,
                    filter
            );
        } else {
            return this.fetchColumns(
                    offset,
                    limit,
                    sortKey,
                    sortColumn,
                    sortDesc,
                    filter
            );
        }
    }
    
    public Column columns(int columnId) throws java.sql.SQLException, javax.ws.rs.WebApplicationException {
        
        String sql = "SELECT c.id, c.name, c.term_count, d.id, d.name, d.columns, d.rows " +
                "FROM " + _config.getColumnsTable() + " c, " + _config.getDatasetsTable() + " d " +
                "WHERE c.dataset = d.id AND c.id = ?";
        
        try (
                Connection con = _config.getConnection();
                PreparedStatement pStmt = con.prepareStatement(sql)
        ) {
            pStmt.setInt(1, columnId);
            try (ResultSet rs = pStmt.executeQuery()) {
                if (rs.next()) {
                    return new Column(
                            rs.getInt(1),
                            rs.getString(2),
                            new Dataset(
                                    rs.getString(4),
                                    rs.getString(5),
                                    rs.getInt(6),
                                    rs.getInt(7),
                                    _config
                            ),
                            rs.getInt(3),
                            _config
                    );
                } else {
                    throw new WebApplicationException(404);
                }
            }
        }
    }
    
    public int columnCount() {
        
        return _config.columnCount();
    }
    
    public ResourceListing<Dataset> datasets(
            int offset,
            int limit,
            String sortKey,
            boolean sortDesc,
            String filter
    ) throws java.sql.SQLException {
        
        String sortColumn;
        if (sortKey.equalsIgnoreCase(Dataset.KEY_NAME)) {
            sortColumn = "name";
        } else if (sortKey.equalsIgnoreCase(Dataset.KEY_COLUMNS)) {
            sortColumn = "columns";
        } else if (sortKey.equalsIgnoreCase(Dataset.KEY_ROWS)) {
            sortColumn = "rows";
        } else {
            throw new java.lang.IllegalArgumentException("Invalid sort key: " + sortKey);
        }
        
        if (!filter.trim().equals("")) {
            return this.queryDatasets(
                    offset,
                    limit,
                    sortKey,
                    sortColumn,
                    sortDesc,
                    filter
            );
        } else {
            return this.fetchDatasets(
                    offset,
                    limit,
                    sortKey,
                    sortColumn,
                    sortDesc,
                    filter
            );
        }
     }

    public Dataset datasets(String identifier) throws java.sql.SQLException {
        
        Dataset result = null;

        String sql = "SELECT id, name, columns, rows " +
                "FROM " + _config.getDatasetsTable() + " " +
                "WHERE id = ?";
        
        try (
                Connection con = _config.getConnection();
                PreparedStatement pStmt = con.prepareStatement(sql)
        ) {
            pStmt.setString(1, identifier);
            try (ResultSet rs = pStmt.executeQuery()) {
                if (rs.next()) {
                    result = new Dataset(
                            rs.getString(1),
                            rs.getString(2),
                            rs.getInt(3),
                            rs.getInt(4),
                            _config
                    );
                }
            }
        }
        
        return result;
    }
    
    public int datasetCount() {
        
        return _config.datasetCount();
    }

    public ResourceListing<Domain> domains(
            int offset,
            int limit,
            String sortKey,
            boolean sortDesc,
            String filter
    ) throws java.sql.SQLException {
        
        String sortColumn;
        if (sortKey.equalsIgnoreCase(Domain.KEY_COLUMN_COUNT)) {
            sortColumn = "column_count";
        } else if (sortKey.equalsIgnoreCase(Domain.KEY_TERM_COUNT)) {
            sortColumn = "term_count";
        } else {
            throw new java.lang.IllegalArgumentException("Invalid sort key: " + sortKey);
        }

	if (!filter.trim().equals("")) {
	    return this.queryDomains(
		    offset,
		    limit,
		    sortKey,
		    sortColumn,
		    sortDesc,
		    filter
	    );
	} else {
	    return this.fetchDomains(
		    offset,
		    limit,
		    sortKey,
		    sortColumn,
		    sortDesc,
		    filter
	    );
	}
    }

    public Domain domains(int domainId) throws java.sql.SQLException, javax.ws.rs.WebApplicationException {
        
        String sql = "SELECT domain_id, column_count, term_count, is_strong " +
                "FROM " + _config.getDomainsTable() + " " +
                "WHERE domain_id = ?";
                
        try (
                Connection con = _config.getConnection();
                PreparedStatement pStmt = con.prepareStatement(sql);
        ) {
            pStmt.setInt(1, domainId);
            try (ResultSet rs = pStmt.executeQuery()) {
                if (rs.next()) {
                    return new Domain(
                            rs.getInt(1),
                            rs.getInt(2),
                            rs.getInt(3),
                            rs.getBoolean(4),
                            _config
                    );
                } else {
                    throw new WebApplicationException(404);
                }
            }
        }
    }
    
    public int domainCount() {
        
        return _config.domainCount();
    }
    
    private ResourceListing<Column> fetchColumns(
            int offset,
            int limit,
            String sortKey,
            String sortColumn,
            boolean sortDesc,
            String filter
    ) throws java.sql.SQLException {
        
        String sql = "SELECT c.id, c.name, c.term_count, d.id, d.name, d.columns, d.rows " +
                "FROM " + _config.getColumnsTable() + " c, " + _config.getDatasetsTable() + " d " +
                "WHERE c.dataset = d.id " +
                "ORDER BY " + sortColumn;
        sql = ApiResource.decorateForPagination(sql, offset, limit, sortDesc);
        
        ResourceListing<Column> columns;
        columns = new ResourceListing<>(
                "columns",
                offset,
                limit,
                _config.columnCount(),
                _config.urls().getColumnsUrl(),
                sortKey,
                sortDesc,
		filter
        );
        
        try (
                Connection con = _config.getConnection();
                Statement stmt = con.createStatement();
                ResultSet rs = stmt.executeQuery(sql)
        ) {
            while (rs.next()) {
                columns.add(
                        new Column(
                            rs.getInt(1),
                            rs.getString(2),
                            new Dataset(
                                    rs.getString(4),
                                    rs.getString(5),
                                    rs.getInt(6),
                                    rs.getInt(7),
                                    _config
                            ),
                            rs.getInt(3),
                            _config
                        )
                );
            }
        }
        return columns;
    }
    
    private ResourceListing<Dataset> fetchDatasets(
            int offset,
            int limit,
            String sortKey,
            String sortColumn,
            boolean sortDesc,
            String filter
    ) throws java.sql.SQLException {
        
        String sql = "SELECT id, name, columns, rows " +
                "FROM " + _config.getDatasetsTable() + " " +
                "ORDER BY " + sortColumn;
        sql = ApiResource.decorateForPagination(sql, offset, limit, sortDesc);
        
        ResourceListing<Dataset> datasets;
        datasets = new ResourceListing<>(
                "datasets",
                offset,
                limit,
                _config.datasetCount(),
                _config.urls().getDatasetsUrl(),
                sortKey,
                sortDesc,
                filter
        );
        
        try (
                Connection con = _config.getConnection();
                Statement stmt = con.createStatement();
                ResultSet rs = stmt.executeQuery(sql)
        ) {
            while (rs.next()) {
                datasets.add(
                        new Dataset(
                                rs.getString(1),
                                rs.getString(2),
                                rs.getInt(3),
                                rs.getInt(4),
                                _config
                        )
                );
            }
        }
        return datasets;
    }

    private ResourceListing<Domain> fetchDomains(
            int offset,
            int limit,
            String sortKey,
            String sortColumn,
            boolean sortDesc,
            String filter
    ) throws java.sql.SQLException {

	String sql = "SELECT domain_id, column_count, term_count, is_strong " +
                "FROM " + _config.getDomainsTable() + " " +
                "ORDER BY " + sortColumn;
        sql = ApiResource.decorateForPagination(sql, offset, limit, sortDesc);

        try (
                Connection con = _config.getConnection();
                Statement stmt = con.createStatement();
                ResultSet rs = stmt.executeQuery(sql)
        ) {
	    return Domain.addDomainsFromResultSet(
		    rs,
		    new ResourceListing<Domain>(
			    "domains",
			    offset,
			    limit,
			    _config.domainCount(),
			    _config.urls().getDatabaseDomainsUrl(),
			    sortKey,
			    sortDesc,
			    filter
		    ),
                    _config
	    );
        }
    }
    
    public String identifier() {
        
        return _config.identifier();
    }

    private ResourceListing<Column> queryColumns(
            int offset,
            int limit,
            String sortKey,
            String sortColumn,
            boolean sortDesc,
            String filter
    ) throws java.sql.SQLException {
        
        String sql;
        List<String> parameters = new ArrayList<>();
        if (filter.toLowerCase().startsWith("contains ")) {
            String subquery = null;
            Scanner s = new Scanner(filter.substring(8).trim()).useDelimiter("\\s*&&\\s*");
            while (s.hasNext()) {
                parameters.add(s.next().toUpperCase());
                String q =
                        "SELECT c.column_id " +
                        "FROM " + _config.getColumnTermsTable() + " c, " +
                        _config.getTermsTable() + " t " +
                        "WHERE c.term_id = t.id AND t.value = ?";
                if (subquery == null) {
                    subquery = q;
                } else {
                    subquery += " INTERSECT " + q;
                }
            }
            sql = 
                "FROM " + _config.getColumnsTable() + " c, " + _config.getDatasetsTable() + " d " +
                "WHERE c.dataset = d.id AND c.id IN(" + subquery + ")";
        } else if (filter.toLowerCase().startsWith("id ")) {
            int columnId = -1;
            try {
                columnId = Integer.parseInt(filter.substring(2).trim());
            } catch (java.lang.NumberFormatException ex) {
            }
            sql = 
                "FROM " + _config.getColumnsTable() + " c, " + _config.getDatasetsTable() + " d " +
                "WHERE c.dataset = d.id AND c.id = " + columnId;
        } else {
            parameters.add(filter.toLowerCase());
            sql = 
                "FROM " + _config.getColumnsTable() + " c, " + _config.getDatasetsTable() + " d " +
                "WHERE c.dataset = d.id AND LOWER(c.name) LIKE ?";
        }

        String query = ApiResource.decorateForPagination(
                "SELECT c.id, c.name, c.term_count, d.id, d.name, d.columns, d.rows " +
                sql + " ORDER BY " + sortColumn,
                offset,
                limit,
                sortDesc
        );
        
        try (Connection con = _config.getConnection()) {
            ResourceListing<Column> columns;
            columns = new ResourceListing<>(
                    "columns",
                    offset,
                    limit,
                    JDBCHelper.getCount(con, "SELECT COUNT(*) " + sql, parameters),
                    _config.urls().getColumnsUrl(),
                    sortKey,
                    sortDesc,
                    filter
            );
            try (PreparedStatement pStmt = con.prepareStatement(query)) {
                int iPara = 1;
                for (String para : parameters) {
                    pStmt.setString(iPara++, para);
                }
                try (ResultSet rs = pStmt.executeQuery()) {
                    while (rs.next()) {
                        columns.add(
                                new Column(
                                    rs.getInt(1),
                                    rs.getString(2),
                                    new Dataset(
                                            rs.getString(4),
                                            rs.getString(5),
                                            rs.getInt(6),
                                            rs.getInt(7),
                                            _config
                                    ),
                                    rs.getInt(3),
                                    _config
                                )
                        );
                    }
                }
            }
            return columns;
        }
    }

    private ResourceListing<Dataset> queryDatasets(
            int offset,
            int limit,
            String sortKey,
            String sortColumn,
            boolean sortDesc,
            String filter
    ) throws java.sql.SQLException {
        
        String sql = 
                "FROM " + _config.getDatasetsTable() + " " +
                "WHERE LOWER(name) LIKE ?";

        String query = ApiResource.decorateForPagination(
                "SELECT id, name, columns, rows " +
                sql + " ORDER BY " + sortColumn,
                offset,
                limit,
                sortDesc
        );
        
        String para = filter.toLowerCase();
        
        try (Connection con = _config.getConnection()) {
            ResourceListing<Dataset> datasets;
            datasets = new ResourceListing<>(
                    "datasets",
                    offset,
                    limit,
                    JDBCHelper.getCount(con, "SELECT COUNT(*) " + sql, para),
                    _config.urls().getDatasetsUrl(),
                    sortKey,
                    sortDesc,
                    filter
            );
            try (PreparedStatement pStmt = con.prepareStatement(query)) {
                pStmt.setString(1, para);
                try (ResultSet rs = pStmt.executeQuery()) {
                    while (rs.next()) {
                        datasets.add(
                                new Dataset(
                                        rs.getString(1),
                                        rs.getString(2),
                                        rs.getInt(3),
                                        rs.getInt(4),
                                        _config
                                )
                        );
                    }
                }
            }
            return datasets;
        }
    }

    private ResourceListing<Domain> queryDomains(
            int offset,
            int limit,
            String sortKey,
            String sortColumn,
            boolean sortDesc,
            String filter
    ) throws java.sql.SQLException {

        String sql;
        List<String> parameters = new ArrayList<>();
        if (filter.toLowerCase().startsWith("contains ")) {
            String subquery = null;
            Scanner s = new Scanner(filter.substring(8).trim()).useDelimiter("\\s*&&\\s*");
            while (s.hasNext()) {
                parameters.add(s.next().toUpperCase());
                String q =
                        "SELECT d.domain_id " +
                        "FROM " + _config.getDomainTermsTable() + " d, " +
                        _config.getTermsTable() + " t " +
                        "WHERE d.term_id = t.id AND t.value = ?";
                if (subquery == null) {
                    subquery = q;
                } else {
                    subquery += " INTERSECT " + q;
                }
            }
            sql = 
                "FROM " +_config.getDomainsTable() + " d " +
		"WHERE d.domain_id IN(" + subquery + ")";
        } else if (filter.toLowerCase().startsWith("id ")) {
            int domainId = -1;
            try {
                domainId = Integer.parseInt(filter.substring(2).trim());
            } catch (java.lang.NumberFormatException ex) {
            }
            sql = 
                "FROM " +_config.getDomainsTable() + " d " +
		"WHERE d.domain_id = " + domainId;
        } else {
            parameters.add(filter.toLowerCase());
            sql = 
                "FROM " +
		    _config.getDomainsTable() + " d, " +
		    _config.getDomainTermsTable() + " m, " +
		    _config.getTermsTable() + " t " +
		"WHERE d.domain_id = m.domain_id AND m.term_id = t.id AND LOWER(t.value) LIKE ? " +
		"GROUP BY d.domain_id, d.column_count, d.term_count";
        }
                
        String query = ApiResource.decorateForPagination(
		"SELECT d.domain_id, d.column_count, d.term_count, d.is_strong " +
                sql + " ORDER BY " + sortColumn,
		offset,
		limit,
		sortDesc
	);

        try (
                Connection con = _config.getConnection();
                PreparedStatement pStmt = con.prepareStatement(query);
        ) {
            int iPara = 1;
            for (String para : parameters) {
                pStmt.setString(iPara++, para);
            }
	    try (ResultSet rs = pStmt.executeQuery()) {
		return Domain.addDomainsFromResultSet(
			rs,
			new ResourceListing<Domain>(
				"domains",
				offset,
				limit,
				JDBCHelper.getCount(con, "SELECT COUNT(*) FROM (SELECT d.domain_id " + sql + ") AS q", parameters),
				_config.urls().getDatabaseDomainsUrl(),
				sortKey,
				sortDesc,
				filter
			),
                        _config
		);
	    }
        }
    }
    
    public String name() {
        
        return _config.name();
    }
    
    public String title() {
    
        return _config.title();
    }
    
    @Override
    public void write(JsonWriter out) throws java.io.IOException {

        out.beginObject();
        out.name("id").value(_config.identifier());
        out.name(KEY_NAME).value(_config.name());
        out.name(KEY_COLUMN_COUNT).value(_config.columnCount());
        out.name(KEY_DATASET_COUNT).value(_config.datasetCount());
        out.name(KEY_DOMAIN_COUNT).value(_config.domainCount());
        out.name("title").value(_config.title());
        out.name("description").value(_config.description());
        if (_config.logo() != null) {
            out.name("logo").value(_config.logo());
        }
        new HATEOASWriter()
                .add("columns", _config.urls().getColumnsUrl())
                .add("datasets", _config.urls().getDatasetsUrl())
                .add("domains", _config.urls().getDatabaseDomainsUrl())
                .write(out);
        out.endObject();
    }
}
