/*
 * Copyright 2018 New York University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.urban.data.api.resources;

import com.google.gson.stream.JsonWriter;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import javax.ws.rs.core.StreamingOutput;

/**
 * Abstract class for API resources.
 * 
 * @author Heiko Mueller <heiko.mueller@nyu.edu>
 */
public abstract class ApiResource implements StreamingOutput {
    
    public static String decorateForPagination(
            String sql,
            int offset,
            int limit,
            boolean sortDesc
    ) {
        if (sortDesc) {
            sql += " DESC";
        }
        if (limit > 0) {
            sql += " LIMIT " + limit;
        }
        if (offset > 0) {
            sql += " OFFSET " + offset;
        }
        return sql;
    }

    public abstract void write(JsonWriter out) throws java.io.IOException;

    @Override
    public void write(OutputStream out) throws java.io.IOException {

	try (JsonWriter writer = new JsonWriter(new OutputStreamWriter(out, "UTF-8"))) {
            this.write(writer);
        }
    }
}
