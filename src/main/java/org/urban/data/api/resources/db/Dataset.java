/*
 * Copyright 2018 New York University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.urban.data.api.resources.db;

import org.urban.data.api.DatabaseConfig;
import com.google.gson.stream.JsonWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import org.urban.data.api.jdbc.JDBCHelper;
import org.urban.data.api.resources.ApiResource;
import org.urban.data.api.resources.HATEOASWriter;
import org.urban.data.api.resources.ResourceListing;

/**
 * Dataset in a database.
 * 
 * Each dataset has a unique identifier and a name.
 * 
 * @author Heiko Mueller <heiko.mueller@nyu.edu>
 */
public class Dataset extends ApiResource {

    public static final String KEY_COLUMNS = "columnCount";
    public static final String KEY_NAME = "name";
    public static final String KEY_ROWS = "rowCount";
    
    private final int _columns;
    private final DatabaseConfig _config;
    private final String _identifier;
    private final String _name;
    private final int _rows;
    
    public Dataset(
            String identifier,
            String name,
            int columns,
            int rows,
            DatabaseConfig config
    ) {
        _identifier = identifier;
        _name = name;
        _columns = columns;
        _rows = rows;
        _config = config;
    }
    
    /**
     * Add one column per row in the given result set to the columns listing.
     * 
     * @param rs
     * @param columns
     * @return
     * @throws java.sql.SQLException 
     */
    private ResourceListing<Column> addColumnFromResult(
            ResultSet rs,
            ResourceListing<Column> columns
    ) throws java.sql.SQLException {
        
        while (rs.next()) {
            columns.add(
                    new Column(
                            rs.getInt(1),
                            rs.getString(2),
                            this,
                            rs.getInt(3),
                            _config
                    )
            );
        }
        return columns;
    }
    
    public ResourceListing<Column> columns(
            int offset,
            int limit,
            String sortKey,
            boolean sortDesc,
            String filter
    ) throws java.sql.SQLException {
        
        String sortColumn;
        if (sortKey.equalsIgnoreCase(Column.KEY_NAME)) {
            sortColumn = "name";
        } else if (sortKey.equalsIgnoreCase(Column.KEY_DATASET)) {
            sortColumn = "dataset";
        } else if (sortKey.equalsIgnoreCase(Column.KEY_TERM_COUNT)) {
            sortColumn = "term_count";
        } else {
            throw new java.lang.IllegalArgumentException("Invalid sort key: " + sortKey);
        }

        String sql = 
                "FROM " + _config.getColumnsTable() + " " +
                "WHERE dataset = '" + _identifier + "'";
        
        if (!filter.trim().equals("")) {
            String para = filter.toLowerCase();
            sql += " AND LOWER(name) LIKE ?";
            String query = ApiResource.decorateForPagination(
                    "SELECT id, name, term_count " +
                    sql + " ORDER BY " + sortColumn,
                    offset,
                    limit,
                    sortDesc
            );
            try (Connection con = _config.getConnection()) {
                try (PreparedStatement pStmt = con.prepareStatement(query)) {
                    pStmt.setString(1, para);
                    try (ResultSet rs = pStmt.executeQuery()) {
                        return this.addColumnFromResult(
                                rs,
                                new ResourceListing<Column>(
                                    "columns",
                                    offset,
                                    limit,
                                    JDBCHelper.getCount(con, "SELECT COUNT(*) " + sql, para),
                                    _config.urls().getDatasetColumnsUrl(_identifier),
                                    sortKey,
                                    sortDesc,
                                    filter
                            )
                        );
                    }
                }
            }
        } else {
            String query = ApiResource.decorateForPagination(
                    "SELECT id, name, term_count " +
                    sql + " ORDER BY " + sortColumn,
                    offset,
                    limit,
                    sortDesc
            );
            try (
                    Connection con = _config.getConnection();
                    Statement stmt = con.createStatement();
                    ResultSet rs = stmt.executeQuery(query)
            ) {
                return this.addColumnFromResult(
                        rs,
                        new ResourceListing<Column>(
                            "columns",
                            offset,
                            limit,
                            _columns,
                            _config.urls().getDatasetColumnsUrl(_identifier),
                            sortKey,
                            sortDesc,
                            filter
                    )
                );
            }
        }
    }
    
    public String identifier() {
        
        return _identifier;
    }
    public String name() {
        
        return _name;
    }
    
    @Override
    public void write(JsonWriter out) throws java.io.IOException {

        out.beginObject();
        out.name("id").value(_identifier);
        out.name(KEY_NAME).value(_name);
        out.name(KEY_COLUMNS).value(_columns);
        out.name(KEY_ROWS).value(_rows);
        new HATEOASWriter()
                .add("columns", _config.urls().getDatasetColumnsUrl(_identifier))
                .add("self", _config.urls().getDatasetUrl(_identifier))
                .write(out);
        out.endObject();
    }
}
