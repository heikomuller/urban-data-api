/*
 * Copyright 2018 New York University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.urban.data.api.resources.db;

import org.urban.data.api.DatabaseConfig;
import com.google.gson.stream.JsonWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import org.urban.data.api.jdbc.JDBCHelper;
import org.urban.data.api.resources.ApiResource;
import org.urban.data.api.resources.DatabaseResource;
import org.urban.data.api.resources.HATEOASWriter;
import org.urban.data.api.resources.ResourceListing;
import org.urban.data.api.resources.TermHATEOASFactory;

/**
 * Descriptor for a domain in a database.
 * 
 * Each domain has a unique identifier, the number of columns the domain occurs
 * in, and the number of terms in the domain.
 * 
 * @author Heiko Mueller <heiko.mueller@nyu.edu>
 */
public class Domain extends DatabaseResource {

    public static final String KEY_COLUMN_COUNT = "columnCount";
    public static final String KEY_IS_STRONGDOMAIN = "isStrong";
    public static final String KEY_TERM_COUNT = "termCount";
    
    private class DomainTermReferenceFactory implements TermHATEOASFactory{

	private final DatabaseConfig _config;
	private final Domain _domain;
	
	public DomainTermReferenceFactory(Domain domain, DatabaseConfig config) {
	
	    _domain = domain;
	    _config = config;
	}
	
	@Override
	public HATEOASWriter getWriter(int termId) {

	    return new HATEOASWriter()
		    .add(
			    "annotations",
			    _config
				    .urls()
				    .getDomainTermAnnotationsUrl(
					    _domain.identifier(),
					    termId
				    )
		    );
	}	
    }
    
    private final int _columnCount;
    private final DatabaseConfig _config;
    private final int _identifier;
    private final boolean _isStrongDomain;
    private final int _termCount;
    
    public Domain(
            int identifier,
            int columnCount,
            int termCount,
            boolean isStrongDomain,
            DatabaseConfig config
    ) {
	super(config);
        _identifier = identifier;
        _columnCount = columnCount;
        _termCount = termCount;
        _isStrongDomain = isStrongDomain;
        _config = config;
    }

    private ResourceListing<Column> addColumnsFromResult(
	    ResultSet rs,
	    ResourceListing<Column> columns
    ) throws java.sql.SQLException {
	
	while (rs.next()) {
	    columns.add(
		    new Column(
			    rs.getInt(1),
			    rs.getString(2),
			    new Dataset(
				    rs.getString(4),
				    rs.getString(5),
				    rs.getInt(6),
				    rs.getInt(7),
				    _config
			    ),
			    rs.getInt(3),
			    _config
		    )
	    );
	}
	
	return columns;
    }
    
    public static ResourceListing<Domain> addDomainsFromResultSet(
	    ResultSet rs,
	    ResourceListing<Domain> domains,
            DatabaseConfig config
    ) throws java.sql.SQLException {
	
	while (rs.next()) {
	    domains.add(
		    new Domain(
			    rs.getInt(1),
			    rs.getInt(2),
			    rs.getInt(3),
                            rs.getBoolean(4),
			    config
		    )
	    );
	}
	
	return domains;
    }
    
    private ResourceListing<Term> addTermsFromResult(
	    ResultSet rs,
	    ResourceListing<Term> terms
    ) throws java.sql.SQLException {
	
	while (rs.next()) {
	    terms.add(
		    new Term(
			    rs.getInt(1),
			    rs.getString(2),
			    new DomainTermReferenceFactory(this, _config)
		    )
	    );
	}
	
	return terms;
    }
    
    public ResourceListing<Column> columns(
            int offset,
            int limit,
            String sortKey,
            boolean sortDesc,
	    String filter
    ) throws java.sql.SQLException {
        
        String sortColumn;
        if (sortKey.equalsIgnoreCase(Column.KEY_NAME)) {
            sortColumn = "c.name";
        } else if (sortKey.equalsIgnoreCase(Column.KEY_DATASET)) {
            sortColumn = "d.name";
        } else if (sortKey.equalsIgnoreCase(Column.KEY_TERM_COUNT)) {
            sortColumn = "c.term_count";
        } else {
            throw new java.lang.IllegalArgumentException("Invalid sort key: " + sortKey);
        }
        
        String sql = 
                "FROM " + _config.getDatasetsTable() + " d, " +
                _config.getColumnsTable() + " c, " +
                _config.getDomainColumnsTable() + " m " +
                "WHERE d.id = c.dataset AND c.id = m.column_id AND m.domain_id = " + _identifier;

	try (Connection con = _config.getConnection()) {
	    if (!filter.trim().equals("")) {
		sql += " AND LOWER(c.name) LIKE ?";
		String query = ApiResource.decorateForPagination(
			"SELECT c.id, c.name, c.term_count, d.id, d.name, d.columns, d.rows " +
			sql + " ORDER BY " + sortColumn,
			offset,
			limit,
			sortDesc
		);
		String para = filter.toLowerCase();
		try (PreparedStatement pStmt = con.prepareStatement(query)) {
		    pStmt.setString(1, para);
		    try (ResultSet rs = pStmt.executeQuery()) {
			return this.addColumnsFromResult(
				rs,
				new ResourceListing<Column>(
					"columns",
					offset,
					limit,
					JDBCHelper.getCount(con, "SELECT COUNT(*) " + sql, para),
					_config.urls().getDomainColumnsUrl(_identifier),
					sortKey,
					sortDesc,
					filter
				)
			);
		    }
		}
	    } else {
		String query = ApiResource.decorateForPagination(
			"SELECT c.id, c.name, c.term_count, d.id, d.name, d.columns, d.rows " +
			sql + " ORDER BY " + sortColumn,
			offset,
			limit,
			sortDesc
		);
		try (
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(query)
		) {
		    return this.addColumnsFromResult(
			    rs,
			    new ResourceListing<Column>(
				    "columns",
				    offset,
				    limit,
				    _columnCount,
				    _config.urls().getDomainColumnsUrl(_identifier),
				    sortKey,
				    sortDesc,
				    filter
			    )
		    );
		}
	    }
	}
    }
    
    public Integer getAnnotation() throws java.sql.SQLException {
        
        return this.getAnnotation(this.getUri());
    }
    
    public Integer getAnnotation(int termId) throws java.sql.SQLException {
        
        return this.getAnnotation(this.getUri(termId));
    }
    
    private String getUri() {
    
        return "urban:domain:" + _identifier;
    }
    
    private String getUri(int termId) {
    
        return this.getUri() + ":" + termId;
    }
    
    public int identifier() {
	
	return _identifier;
    }
    
    public void setAnnotation(Integer value) throws java.sql.SQLException {
        
        this.setAnnotation(this.getUri(), value);
    }
    
    public void setAnnotation(int termId, Integer value) throws java.sql.SQLException {
        
        this.setAnnotation(this.getUri(termId), value);
    }

    public ResourceListing<Term> terms(
            int offset,
            int limit,
            boolean sortDesc,
	    String filter
    ) throws java.sql.SQLException {
        
        String sql = 
                "FROM " + _config.getTermsTable() + " t, " +
                _config.getDomainTermsTable() + " d " +
                "WHERE t.id = d.term_id AND d.domain_id = " + _identifier;
	
	try (Connection con = _config.getConnection()) {
	    if (!filter.trim().equals("")) {
		sql += " AND LOWER(t.value) LIKE ?";
		String query = ApiResource.decorateForPagination(
			"SELECT t.id, t.value " + sql + " ORDER BY t.value",
			offset,
			limit,
			sortDesc
		);
		String para = filter.toLowerCase();
		try (PreparedStatement pStmt = con.prepareStatement(query)) {
		    pStmt.setString(1, para);
		    try (ResultSet rs = pStmt.executeQuery()) {
		    return this.addTermsFromResult(
			    rs,
			    new ResourceListing<Term>(
				    "terms",
				    offset,
				    limit,
				    JDBCHelper.getCount(con, "SELECT COUNT(*) " + sql, para),
				    _config.urls().getDomainTermsUrl(_identifier),
				    null,
				    sortDesc,
				    filter
			    )
		    );
		    }
		}
	    } else {
		String query = ApiResource.decorateForPagination(
			"SELECT t.id, t.value " + sql + " ORDER BY t.value",
			offset,
			limit,
			sortDesc
		);
		try (
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(query)
		) {
		    return this.addTermsFromResult(
			    rs,
			    new ResourceListing<Term>(
				    "terms",
				    offset,
				    limit,
				    _termCount,
				    _config.urls().getDomainTermsUrl(_identifier),
				    null,
				    sortDesc,
				    filter
			    )
		    );
		}
	    }
	}
    }
    
    @Override
    public void write(JsonWriter out) throws java.io.IOException {

        out.beginObject();
        out.name("id").value(_identifier);
        out.name(KEY_COLUMN_COUNT).value(_columnCount);
        out.name(KEY_TERM_COUNT).value(_termCount);
        out.name(KEY_IS_STRONGDOMAIN).value(_isStrongDomain);
        out.name("database");
        out.beginObject();
        out.name("id").value(_config.identifier());
        out.name("name").value(_config.name());
        out.name("title").value(_config.title());
        out.endObject();
        new HATEOASWriter()
                .add("annotations", _config.urls().getDomainAnnotationsUrl(_identifier))
                .add("columns", _config.urls().getDomainColumnsUrl(_identifier))
		.add("self", _config.urls().getDomainUrl(_identifier))
                .add("terms", _config.urls().getDomainTermsUrl(_identifier))
                .write(out);
        out.endObject();
    }
}
