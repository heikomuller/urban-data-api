/*
 * Copyright 2018 New York University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.urban.data.api.resources;

import com.google.gson.stream.JsonWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.StreamingOutput;

/**
 * A listing of Api resources.
 * 
 * @author Heiko Mueller <heiko.mueller@nyu.edu>
 * @param <T>
 */
public class ResourceListing <T extends ApiResource> implements StreamingOutput {

    private final int _count;
    private final List<T> _elements;
    private final String _label;
    private final int _limit;
    private final HATEOASWriter _links;
    private final HashMap<String, String> _navigate;
    private final int _offset;
    
    public ResourceListing(
            String label,
            int offset,
            int limit,
            int count,
            String url,
            String sortKey,
            boolean sortDesc,
            String filter,
            HATEOASWriter links
    ) {
        
        _label = label;
        _offset = offset;
        _limit = limit;
        _count = count;
        _links = links;
        
        _elements = new ArrayList<>();
        _navigate = new HashMap<>();
        
        String suffix = "";
        if (sortKey != null) {
            suffix = "&sortKey=" + sortKey;
        }
        suffix +=  "&sortDesc=" + Boolean.toString(sortDesc);
        if (!filter.equals("")) {
            try {
                suffix += "&filter=" + URLEncoder.encode(filter, "UTF-8");
            } catch (java.io.UnsupportedEncodingException ex) {
                Logger.getGlobal().log(Level.WARNING, "ENCODE QUERY STRING", ex);
            }
        }
        _navigate.put("full", url);
        if (offset > 0) {
            _navigate.put("first", url + "?offset=0&limit=" + limit + suffix);
            if (limit > 0) {
                int prevOffset = Math.max(0, offset - limit);
                _navigate.put("prev", url + "?offset=" + prevOffset + "&limit=" + limit + suffix);
            }
        }
        if (limit > 0) {
            int lastElement = Math.max(0, offset) + limit;
            if (lastElement < count) {
                _navigate.put("last", url + "?offset=" + (count - limit) + "&limit=" + limit + suffix);
                int nextOffset = Math.max(0, offset) + limit;
                _navigate.put("next", url + "?offset=" + nextOffset + "&limit=" + limit + suffix);
            }
        }
    }
    
    public ResourceListing(
            String label,
            int offset,
            int limit,
            int count,
            String url,
            String sortKey,
            boolean sortDesc,
            String filter
    ) {
        
        this(label, offset, limit, count, url, sortKey, sortDesc, filter, null);
    }
    
    public ResourceListing<T> add(T element) {
        
        _elements.add(element);
        
        return this;
    }
    
    public int size() {
        
        return _elements.size();
    }
    
    @Override
    public void write(OutputStream out) throws IOException, WebApplicationException {

	try (JsonWriter writer = new JsonWriter(new OutputStreamWriter(out, "UTF-8"))) {
	    writer.beginObject();
            writer.name(_label);
            writer.beginArray();
            for (T element : _elements) {
                element.write(writer);
            }
            writer.endArray();
            // Pagination Urls
            writer.name("navigate");
            writer.beginObject();
            for (String key : _navigate.keySet()) {
                writer.name(key).value(_navigate.get(key));
            }
            if (_offset >= 0) {
                writer.name("offset").value(_offset);
            }
            if (_limit >= 0) {
                writer.name("limit").value(_limit);
            }
            writer.name("count").value(_count);
            writer.endObject();
            // HATEOAS references
            if (_links != null) {
                _links.write(writer);
            }
            writer.endObject();
        }
    }
}
