/*
 * Copyright 2018 New York University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.urban.data.api.resources;

import com.google.gson.stream.JsonWriter;
import java.util.HashMap;

/**
 * Write list of references to Json output stream.
 * 
 * @author Heiko Mueller <heiko.mueller@nyu.edu>
 */
public class HATEOASWriter {
   
    private final HashMap<String, String> _links = new HashMap<>();
    
    public HATEOASWriter add(String rel, String href) {
    
        _links.put(rel, href);
        return this;
    }
    
    public void write(JsonWriter out) throws java.io.IOException {
        
        out.name("links");
        out.beginArray();
        for (String key : _links.keySet()) {
            out.beginObject();
            out.name("rel").value(key);
            out.name("href").value(_links.get(key));
            out.endObject();
        }
        out.endArray();
    }
}
