/*
 * Copyright 2018 New York University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.urban.data.api.urls;

/**
 * Factory for API resource Urls.
 * 
 * @author Heiko Mueller <heiko.mueller@nyu.edu>
 */
public class DatabaseUrlFactory extends UrlFactory {
    
    private final String _databaseId;
    
    public DatabaseUrlFactory(String databaseId, String baseUrl) {
        
        super(baseUrl);
        
        _databaseId = databaseId;
    }
    
    public String getColumnsUrl() {
        
        return this.getDatabaseUrl(_databaseId) + "/columns";
    }
    
    public String getColumnUrl(int columnId) {
        
        return this.getColumnsUrl() + "/" + columnId;
    }
    
    public String getColumnAnnotationsUrl(int columnId) {
        
        return this.getColumnUrl(columnId) + "/annotations";
    }
    
    public String getColumnDomainsUrl(int columnId) {
        
        return this.getColumnUrl(columnId) + "/domains";
    }
        
    public String getColumnTermsUrl(int columnId) {
        
        return this.getColumnUrl(columnId) + "/terms";
    }
    
    public String getColumnTermAnnotationsUrl(int columnId, int termId) {
        
        return this.getColumnTermsUrl(columnId) + "/" + termId + "/annotations";
    }
    
    public String getDatasetsUrl() {
        
        return this.getDatabaseUrl(_databaseId) + "/datasets";
    }
    
    public String getDatasetUrl(String datasetId) {
        
        return this.getDatasetsUrl() + "/" + datasetId;
    }
    
    public String getDatasetColumnsUrl(String datasetId) {
        
        return this.getDatasetUrl(datasetId) + "/columns";
    }

    public String getDomainUrl(int domainId) {
        
        return this.getDatabaseUrl(_databaseId) + "/domains/" + domainId;
    }

    public String getDomainAnnotationsUrl(int domainId) {
        
        return this.getDomainUrl(domainId) + "/annotations";
    }
    
    public String getDomainColumnsUrl(int domainId) {
        
        return this.getDomainUrl(domainId) + "/columns";
    }
    
    public String getDomainTermsUrl(int domainId) {
        
        return this.getDomainUrl(domainId) + "/terms";
    }
    
    public String getDomainTermAnnotationsUrl(int domainId, int termId) {
        
        return this.getDomainUrl(domainId) + "/terms/" + termId + "/annotations";
    }
    
    public String getDatabaseDomainsUrl() {
        
        return this.getDatabaseUrl(_databaseId) + "/domains";
    }
}
