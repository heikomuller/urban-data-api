/*
 * Copyright 2018 New York University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.urban.data.api.jdbc;

import java.sql.Connection;

/**
 * Connector for data stores that support JDBC.
 * 
 * Used to open a new connection to the data store.
 * 
 * @author Heiko Mueller <heiko.mueller@nyu.edu>
 */
public interface JDBCConnector {
    
    /**
     * Get a new connection to the underlying data store.
     * 
     * @return
     * @throws java.sql.SQLException 
     */
    public Connection getConnection() throws java.sql.SQLException;
}
