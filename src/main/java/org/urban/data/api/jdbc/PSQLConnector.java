/*
 * Copyright 2018 New York University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.urban.data.api.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * JDBC connector for PostgreSQL.
 * 
 * @author Heiko Mueller <heiko.mueller@nyu.edu>
 */
public class PSQLConnector implements JDBCConnector {

    /**
     * Number of connection attempts before getConnection() fails.
     * 
     */
    private static final int MAX_ATTEMPT = 5;
    
    /**
     * Time in ms to wait before re-attempting connection after failure.
     * 
     */
    private static final int WAIT_MS = 1000;

    private final String _password;
    private final String _url;
    private final String _user;

    public PSQLConnector(
            String url,
            String user,
            String password
    ) throws java.lang.ClassNotFoundException {

	// Load the PostgreSQL JSOB Driver
	Class.forName("org.postgresql.Driver");

        _url = url;
        _user = user;
        _password = password;
    }
    
    @Override
    public Connection getConnection() throws SQLException {

	SQLException exception = null;

	for (int iAttempt = 0; iAttempt < MAX_ATTEMPT; iAttempt++) {
	    try {
		return DriverManager.getConnection(_url, _user, _password);
	    } catch (java.sql.SQLException sqlException) {
		System.out.println(_url);
		Logger.getGlobal().log(Level.SEVERE, _url, exception);
		exception = sqlException;
		try {
		    Thread.sleep(WAIT_MS);
		} catch (java.lang.InterruptedException ie) {
		}
	    }
	}
	
	// If we reach this part connecting failed MAX_ATTEMPT times. The
	// exception is the SQLException of the last failed attempt.
	throw exception;
    }
}
