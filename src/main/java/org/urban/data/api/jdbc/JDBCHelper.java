/*
 * Copyright 2018 New York University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.urban.data.api.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * Helper methods for JDBC queries.
 * 
 * @author Heiko Mueller <heiko.mueller@nyu.edu>
 */
public final class JDBCHelper {
   
    /**
     * Expects a query that returns a single row with a single column.
     * 
     * The most common use is for SELECT COUNT(*) ... queries. Returns the value
     * of the first column in the first row of the result set as integer.
     * 
     * @param con
     * @param query
     * @return 
     * @throws java.sql.SQLException 
     */
    public static int getCount(Connection con, String query) throws java.sql.SQLException {
        
        try (
                Statement stmt = con.createStatement();
                ResultSet rs = stmt.executeQuery(query);
        ) {
            rs.next();
            return rs.getInt(1);
        }
    }
   
    /**
     * Expects a query that returns a single row with a single column.
     * 
     * The most common use is for SELECT COUNT(*) ... queries. Returns the value
     * of the first column in the first row of the result set as integer. Uses
     * a prepared statement and expects a single argument.
     * 
     * @param con
     * @param query
     * @param argument
     * @return 
     * @throws java.sql.SQLException 
     */
    public static int getCount(Connection con, String query, String argument) throws java.sql.SQLException {
        
        try (PreparedStatement pStmt = con.prepareStatement(query)) {
            pStmt.setString(1, argument);
            try (ResultSet rs = pStmt.executeQuery()) {
                rs.next();
                return rs.getInt(1);
            }
        }
    }
   
    /**
     * Expects a query that returns a single row with a single column.
     * 
     * The most common use is for SELECT COUNT(*) ... queries. Returns the value
     * of the first column in the first row of the result set as integer. Uses
     * a prepared statement and expects a list of arguments.
     * 
     * @param con
     * @param query
     * @param arguments
     * @return 
     * @throws java.sql.SQLException 
     */
    public static int getCount(Connection con, String query, Iterable<String> arguments) throws java.sql.SQLException {
        
        try (PreparedStatement pStmt = con.prepareStatement(query)) {
            int iArg = 1;
            for (String arg : arguments) {
                pStmt.setString(iArg++, arg);
            }
            try (ResultSet rs = pStmt.executeQuery()) {
                rs.next();
                return rs.getInt(1);
            }
        }
    }
}
