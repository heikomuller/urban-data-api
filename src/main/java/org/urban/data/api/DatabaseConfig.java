/*
 * Copyright 2018 New York University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.urban.data.api;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import org.urban.data.api.jdbc.JDBCConnector;
import org.urban.data.api.jdbc.PSQLConnector;
import org.urban.data.api.urls.DatabaseUrlFactory;

/**
 *
 * @author Heiko Mueller <heiko.mueller@nyu.edu>
 */
public class DatabaseConfig implements JDBCConnector {
    
    private static final String ANNOTATIONS = "annotations";
    private static final String COLUMNS = "columns";
    private static final String COLUMN_TERMS = "columnTermMap";
    private static final String DATASETS = "datasets";
    private static final String DOMAINS = "domains";
    private static final String DOMAIN_COLUMNS = "domainColumnMap";
    private static final String DOMAIN_TERMS = "domainTermMap";
    private static final String TERMS = "terms";
    
    private final int _columnCount;
    private final JDBCConnector _connector;
    private final int _datasetCount;
    private final String _description;
    private final int _domainCount;
    private final String _identifier;
    private final String _logo;
    private final String _name;
    private final HashMap<String, String> _tables;
    private final String _title;
    private final DatabaseUrlFactory _urls;
    
    public DatabaseConfig(JsonObject config, String url) throws java.lang.ClassNotFoundException, java.sql.SQLException {
    
        _connector = new PSQLConnector(
            config.get("connector")
                    .getAsJsonObject()
                    .get("url")
                    .getAsString(),
                config.get("connector")
                        .getAsJsonObject()
                        .get("user")
                        .getAsString(),
            config.get("connector")
                    .getAsJsonObject()
                    .get("password")
                    .getAsString()                
        );

        _identifier = config.get("id").getAsString();
        _name = config.get("name").getAsString();
        _title = config.get("title").getAsString();
        _description = config.get("description").getAsString();
        if (config.has("logo")) {
            _logo = config.get("logo").getAsString();
        } else {
            _logo = null;
        }
        
        _urls = new DatabaseUrlFactory(config.get("id").getAsString(), url);
        
        _tables = new HashMap<>();
        for (JsonElement el : config.get("tables").getAsJsonArray()) {
            JsonObject obj = el.getAsJsonObject();
            _tables.put(
                    obj.get("key").getAsString(),
                    obj.get("name").getAsString()
            );
        }
        
        try (
                Connection con = _connector.getConnection();
                Statement stmt = con.createStatement();
        ) {
            String sql = "SELECT COUNT(*) FROM " + this.getDomainsTable();
            try (ResultSet rs = stmt.executeQuery(sql)) {
                rs.next();
                _domainCount = rs.getInt(1);
            }
            sql = "SELECT COUNT(*) FROM " + this.getDatasetsTable();
            try (ResultSet rs = stmt.executeQuery(sql)) {
                rs.next();
                _datasetCount = rs.getInt(1);
            }
            sql = "SELECT COUNT(*) FROM " + this.getColumnsTable();
            try (ResultSet rs = stmt.executeQuery(sql)) {
                rs.next();
                _columnCount = rs.getInt(1);
            }
        }
    }
    
    public void add(String key, String value) {
        
        _tables.put(key, value);
    }
    
    public int columnCount() {
        
        return _columnCount;
    }
    
    public boolean contains(String key) {
        
        return _tables.containsKey(key);
    }
    
    public int datasetCount() {
        
        return _datasetCount;
    }
    
    public String description() {
        
        return _description;
    }
    
    public int domainCount() {
        
        return _domainCount;
    }
    
    public String get(String key) {
        
        return _tables.get(key);
    }
    
    public final String getAnnotationsTable() {
        
        return _tables.get(ANNOTATIONS);
    }
    
    public final String getColumnsTable() {
        
        return _tables.get(COLUMNS);
    }
    
    public String getColumnTermsTable() {
        
        return _tables.get(COLUMN_TERMS);
    }

    @Override
    public Connection getConnection() throws java.sql.SQLException {

        return _connector.getConnection();
    }
    
    public final String getDatasetsTable() {
        
        return _tables.get(DATASETS);
    }
    
    public final String getDomainsTable() {
        
        return _tables.get(DOMAINS);
    }
    
    public String getDomainColumnsTable() {
        
        return _tables.get(DOMAIN_COLUMNS);
    }
    
    public String getDomainTermsTable() {
        
        return _tables.get(DOMAIN_TERMS);
    }
    
    public String getTermsTable() {
        
        return _tables.get(TERMS);
    }
    
    public String identifier() {
        
        return _identifier;
    }
    
    public String logo() {
        
        return _logo;
    }
    
    public String name() {
        
        return _name;
    }
    
    public String title() {
        
        return _title;
    }
    
    public DatabaseUrlFactory urls() {
        
        return _urls;
    }
}
